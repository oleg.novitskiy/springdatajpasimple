package com.spduniversity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJpaSimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJpaSimpleApplication.class, args);
	}
}
