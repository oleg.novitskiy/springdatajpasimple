package com.spduniversity;

import com.spduniversity.domain.Category;
import com.spduniversity.repository.CategoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;
import static org.springframework.data.domain.Sort.Direction.ASC;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryRepositoryTest {
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    CategoryRepository repository;

    // Need for testing
    private Category mobilePhones, laptops, monitors;
    private Long id;

    private void flushCategories() {
        mobilePhones = new Category("mobilePhones");
        laptops = new Category("laptops");
        monitors = new Category("Monitors");

        this.repository.save(mobilePhones);
        this.repository.save(laptops);
        this.repository.save(monitors);

        id = mobilePhones.getId();

        assertThat(id, is(notNullValue()));
        assertThat(laptops.getId(), is(notNullValue()));
        assertThat(monitors.getId(), is(notNullValue()));

        assertThat(repository.exists(mobilePhones.getId()), is(true));
        assertThat(repository.exists(laptops.getId()), is(true));
        assertThat(repository.exists(monitors.getId()), is(true));
    }

    @Test
    public void createTest() throws Exception {
        final List<Category> allCategories = this.repository.findAll();
        Long count = allCategories.stream().count();

        flushCategories();

        assertThat(this.repository.count(), is(count + 2));
    }

    @Test
    public void testRead() throws Exception {
        flushCategories();

        Category foundCategory = repository.findOne(id);
        assertThat(mobilePhones.getName(), is(foundCategory.getName()));
    }

    @Test
    public void testReadByIdReturnsNullForNotFoundEntities() throws Exception {
        flushCategories();

        assertThat(repository.findOne(id * 11), is(nullValue()));
    }

    @SuppressWarnings("RedundantCast")
    @Test
    public void testSavingNullCollectionIsNoOp() throws Exception {
        List<Category> result = repository.save((Collection<Category>)null);
        assertThat(result, is(notNullValue()));
        assertThat(result.isEmpty(), is(true));
    }

    @Test
    public void testSavingEmptyCollectionIsNoOp() throws Exception {
        List<Category> result = repository.save(new ArrayList<>());
        assertThat(result, is(notNullValue()));
        assertThat(result.isEmpty(), is(true));
    }

    @Test
    public void testUpdate() throws Exception {
        flushCategories();

        Category foundCategory = repository.findOne(id);
        foundCategory.setName("super category");

        Category updatedCategory = repository.findOne(id);
        assertThat(updatedCategory.getName(), is(foundCategory.getName()));
    }

    @Test
    public void testDelete() throws Exception {
        flushCategories();

        repository.delete(mobilePhones);
        assertThat(repository.exists(id), is(false));
        assertThat(repository.findOne(id), is(nullValue()));
    }

    @Test
    public void testDeleteACategoryById() throws Exception {
        flushCategories();

        repository.delete(mobilePhones.getId());
        assertThat(repository.exists(id), is(false));
        assertThat(repository.findOne(id), is(nullValue()));
    }

    @Test
    public void testFindAllByGivenIds() throws Exception {
        flushCategories();

        Iterable<Category> result = repository.findAll(Arrays.asList(mobilePhones.getId(), laptops.getId()));
        assertThat(result, hasItems(mobilePhones, laptops));
    }

    @Test
    public void testReadAll() throws Exception {
        flushCategories();

        assertThat(repository.count(), is(3L));
        assertThat(repository.findAll(), hasItems(mobilePhones, laptops, monitors));
    }

    @Test
    public void testDeleteAll() throws Exception {
        flushCategories();

        repository.deleteAll();
        assertThat(repository.count(), is(0L));
    }

    @Test
    public void testDeleteAllInBatch() throws Exception {
        flushCategories();

        repository.deleteAllInBatch();
        assertThat(repository.count(), is(0L));
    }

    @Test
    public void testReturnsAllSortedCorrectly() throws Exception {
        flushCategories();

        List<Category> result = repository.findAll(new Sort(ASC, "name"));
        assertThat(result, is(notNullValue()));
        assertThat(result.size(), is(3));
        assertThat(result.get(0), is(monitors));
        assertThat(result.get(1), is(laptops));
        assertThat(result.get(2), is(mobilePhones));
    }

    @Test
    public void testReturnsIgnoreCaseAllSortedCorrectly() throws Exception {
        flushCategories();

        Sort.Order order = new Sort.Order(ASC, "name").ignoreCase();
        List<Category> result = repository.findAll(new Sort(order));
        assertThat(result, is(notNullValue()));
        assertThat(result.size(), is(3));
        assertThat(result.get(0), is(laptops));
        assertThat(result.get(1), is(mobilePhones));
        assertThat(result.get(2), is(monitors));
    }

    @Test
    public void testFindByName() throws Exception {
        flushCategories();

        List<Category> byName = repository.findByName("mobilePhones");

        assertThat(byName.size(), is(1));
        assertThat(byName.get(0), is(mobilePhones));
    }

    @Test
    public void testFindByNameNotFound() throws Exception {
        flushCategories();

        List<Category> byName = repository.findByName("myPhones");

        assertThat(byName.size(), is(0));
        assertThat(byName.isEmpty(), is(true));
    }

    @Test
    public void testCountsCorrectly() throws Exception {
        long count = repository.count();

        Category category = new Category("planes");
        repository.save(category);

        assertThat(repository.count() == count + 1, is(true));
    }

    @Test
    public void testExecutesNotLikeCorrectly() throws Exception {
        flushCategories();

        List<Category> result = repository.findByNameNotLike("%ps1%");
        assertThat(result.size(), is(3));
        assertThat(result, hasItems(mobilePhones, laptops, monitors));
    }
}